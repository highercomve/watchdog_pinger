FROM --platform=$BUILDPLATFORM golang:1.22.3-alpine3.18 as builder

ARG TARGETPLATFORM
ARG BUILDPLATFORM

RUN apk update && \
  apk add make upx

WORKDIR /app
COPY go.mod /app/
COPY go.sum /app/
COPY main.go /app/
COPY build.sh /app/
COPY Makefile /app/Makefile

RUN /app/build.sh

FROM scratch

WORKDIR /app
VOLUME [ "/tmp" ]

# RUN apk update && apk add dbus

COPY --from=builder /app/watchdog_pinger /app/watchdog_pinger

CMD [ "/app/watchdog_pinger" ]
