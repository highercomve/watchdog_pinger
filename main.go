package main

import (
	"fmt"
	"os"
	"strconv"
	"time"

	"golang.org/x/sys/unix"
)

// Define the watchdog device file
const (
	WATCHDOG_DEVICE = "/dev/watchdog"
)

func readMeta(t, key string) (value string, err error) {
	filename := fmt.Sprintf("/pantavisor/%s-meta/pvwg.%s", t, key)
	_, err = os.Stat(filename)
	if err != nil {
		return value, err
	}
	content, err := os.ReadFile(filename)
	if err != nil {
		return value, err
	}

	return string(content), nil
}

func readUserMeta(key string) (value string, err error) {
	return readMeta("user", key)
}

func getDisabled() (bool, error) {
	result, err := readUserMeta("disabled")
	if err != nil {
		return false, err
	}
	return strconv.ParseBool(result)
}

func getPingInterval(defaultInterval int) (int, error) {
	pi, err := readUserMeta("ping-interval")
	if err != nil {
		return defaultInterval, err
	}
	return strconv.Atoi(pi)
}

// Function to get the watchdog timeout
func getWatchdogTimeout(fd int) int {
	s, err := unix.IoctlGetInt(fd, unix.WDIOC_GETTIMEOUT)
	if err != nil {
		return 10
	}

	return s
}

// Function to ping the watchdog
func pingWatchdog(fd int) error {
	return unix.IoctlWatchdogKeepalive(fd)
}

// Function to get current UTC time as a string
func getUTCTime() string {
	now := time.Now().UTC()
	return now.Format(time.RFC3339)
}

func main() {
	// Open the watchdog device
	fd, err := os.OpenFile(WATCHDOG_DEVICE, os.O_RDWR, 0)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to open watchdog device: %v\n", err)
		os.Exit(1)
	}
	defer fd.Close()

	// Get the watchdog timeout
	timeout := getWatchdogTimeout(int(fd.Fd()))
	defaultInterval := timeout / 2

	disabled := false
	pingInterval, err := getPingInterval(defaultInterval)
	if err == nil {
		pingInterval = defaultInterval
	}
	is_debug := os.Getenv("DEBUG") == "true"
	print_message := true

	fmt.Printf("Watchdog pinger started. Timeout is %d seconds. Pinging every %d seconds.\n", timeout, pingInterval)
	fmt.Printf("Debug mode if set to %t.\n", is_debug)

	// Pinging loop
	for {
		pingInterval, err = getPingInterval(defaultInterval)
		if err == nil {
			pingInterval = defaultInterval
		}
		disabled, err = getDisabled()
		if err != nil {
			disabled = false
		}
		if disabled {
			fmt.Printf("Watchdog pinger disabled.\n")
			continue
		}

		pingWatchdog(int(fd.Fd()))

		if print_message {
			utcTime := getUTCTime()
			fmt.Printf("Watchdog pinged at %s.\n", utcTime)
		}

		if print_message && !is_debug {
			print_message = false
		}
		time.Sleep(time.Duration(pingInterval) * time.Second)
	}
}
