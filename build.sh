#!/bin/sh
set -e

echo "Building for $TARGETPLATFORM" 

target=""
case "$TARGETPLATFORM" in
	"linux/arm/v6"*)
		target="linux-arm-6"
		;;
	"linux/arm/v7"*)
		target="linux-arm-7"
		;;
	"linux/arm")
		target="linux-arm"
		;;
	"linux/arm64"*)
		target="linux-arm64"
		;;
	"linux/386"*)
		target="linux-386"
		;;
	"linux/amd64"*)
		target="linux-amd64"
		;;
	"linux/mips"*)
		target="linux-mips"
		;;
	"linux/mipsle"*)
		target="linux-mipsle"
		;;
	"linux/mips64"*)
		target="linux-mips64"
		;;
	"linux/mips64le"*)
		target="linux-mips64le"
		;;
	"linux/riscv64"*)
		target="linux-riscv64"
		;;
	*)
		echo "Unknown machine type: $TARGETPLATFORM"
		exit 1
esac

go mod download
OUTPUT_DIR="./" OPTIMIZE=1 make build-$target